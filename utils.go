package main

import (
	"github.com/google/go-github/github"
	"sort"
)

var GitHubClient *github.Client

type Repos struct {
	Name             string           `json:"name"`
	URL              string           `json:"url"`
	LastCommitDate   github.Timestamp `json:"last_commit_date"`
	LastCommitAuthor string           `json:"last_commit_author,omitempty"`
}

// ByLastCommit implements sort.Interface for []Repos based on the LastCommitDate field
type ByLastCommit []Repos

func (a ByLastCommit) Len() int {
	return len(a)
}

func (a ByLastCommit) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByLastCommit) Less(i, j int) bool {
	return a[i].LastCommitDate.Unix() < a[j].LastCommitDate.Unix()
}

// InitUtils - Initializes helper utils
func InitUtils() {
	GitHubClient = github.NewClient(nil)
}

// GetUserRepos - Returns user's public github repositories
func GetUserRepos(username string, sortedByLastCommit bool) ([]Repos, error) {
	opt := &github.RepositoryListOptions{Type: "public"}
	repos, _, err := GitHubClient.Repositories.List(username, opt)
	if err != nil {
		return nil, err
	}
	result := make([]Repos, len(repos))
	// Extract only the required fields
	for index, repo := range repos {
		result[index].Name = *repo.Name
		result[index].URL = *repo.HTMLURL
		result[index].LastCommitDate = *repo.UpdatedAt
	}
	if sortedByLastCommit {
		sort.Sort(ByLastCommit(result))
	}
	return result, nil
}
