package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func storeUsername(c *gin.Context) {
	c.Request.ParseForm()
	username := c.Request.Form.Get("username")
	err := AddUser(username)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"response": "false", "error": err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"response": "true"})
	}
}

func getUserdata(c *gin.Context) {
	c.Request.ParseForm()
	name := c.Params.ByName("name")
	found, err := CheckUser(name)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"response": "false", "error": err.Error()})
		return
	}
	if found == false {
		msg := fmt.Sprintf(`User $v not found in database`, name)
		c.JSON(http.StatusBadRequest, gin.H{"response": "false", "error": msg})
		return
	}
	isOrdered := false
	ordered := c.Request.Form.Get("ordered")
	if ordered == "true" {
		isOrdered = true
	}
	// Fetch data from Github
	repos, err := GetUserRepos(name, isOrdered)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"response": "false", "error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, repos)
}

func main() {
	// Init DB
	db, err := InitDB("sqlite3", "db.sqlite")
	if err != nil {
		log.Fatal("main: ", "Error initializing database: ", err)
		return
	}
	defer db.Close()

	// GitHub Client
	InitUtils()

	// Init HTTP Server
	r := gin.Default()

	r.POST("/", storeUsername)
	r.GET("/:name", getUserdata)

	r.Run(":8080")
}
