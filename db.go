package main

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
)

var db *sql.DB

func InitDB(driver, dbname string) (*sql.DB, error) {
	var err error
	createDb := false
	if _, err := os.Stat(dbname); os.IsNotExist(err) {
		log.Printf("InitDB: database does not exist %s", dbname)
		createDb = true
	}
	db, err = sql.Open(driver, dbname)
	if createDb {
		if err = syncDB(); err != nil {
			return nil, errors.New("Error creating database")
		}
	}
	return db, nil
}

func syncDB() error {
	log.Println("--- syncDB")
	//create users table
	createUsers := `CREATE TABLE users (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                username VARCHAR(255) UNIQUE NOT NULL,
                created_at TIMESTAMP default CURRENT_TIMESTAMP,
                updated_at TIMESTAMP
            );`
	_, err := db.Exec(createUsers, nil)
	return err
}

func AddUser(username string) error {
	log.Println("AddUser", username)
	stmt, err := db.Prepare("INSERT INTO users(username) VALUES(?)")
	if err != nil {
		log.Println("AddUser", err)
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(username)
	if err != nil {
		log.Println("AddUser", err.Error())
		return err
	}
	return nil
}

func CheckUser(username string) (bool, error) {
	log.Println("GetUser", username)
	userFound := false
	query := fmt.Sprintf(`SELECT COUNT(username) FROM users WHERE username="%v"`, username)
	rows, err := db.Query(query)
	if err != nil {
		log.Println("GetUser", err)
		return userFound, err
	}
	defer rows.Close()

	var count int
	for rows.Next() {
		rows.Scan(&count)
	}
	rows.Close()
	userFound = (count > 0)
	return userFound, nil
}
